<?php

require_once 'src/class/disc.php';
require_once 'src/class/tower.php';
require_once 'src/class/game_exception.php';
require_once 'src/class/game.php';

class GameTest extends PHPUnit\Framework\TestCase
{
    public function testMove()
    {
        $game = new Game();
        $game->init();

        $tower1 = $game->getTower(0);
        $tower2 = $game->getTower(1);

        $disc1 = new Disc(2);
        $disc2 = new Disc(4);

        $tower1->push($disc1);
        $tower2->push($disc2);

        $game->move(1, 2);

        $this->assertEquals($disc1, $tower2->peek());
        $this->assertEquals($disc2, $tower1->peek());
    }

    public function testIsOver()
    {
        $game = new Game();
        $game->init();

        $tower1 = $game->getTower(0);
        $tower2 = $game->getTower(1);
        $tower3 = $game->getTower(2);

        for ($i = 1; $i <= 7; $i++) {
            $tower3->push(new Disc($i));
        }

        $this->assertTrue($game->isOver());
    }

    public function testGetTurn()
    {
        $game = new Game();
        $game->init();

        $this->assertEquals(0, $game->getTurn());

        $game->move(1, 2);
        $game->move(1, 3);

        $this->assertEquals(2, $game->getTurn());
    }
}