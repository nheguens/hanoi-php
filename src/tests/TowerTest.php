<?php declare(strict_types=1);


require_once 'src/class/disc.php';
require_once 'src/class/tower.php';

class TowerTest extends PHPUnit\Framework\TestCase
{
    public function testPushAndPop()
    {
        $tower = new Tower();
        $disc = new Disc(3);

        $tower->push($disc);
        $this->assertEquals($disc, $tower->pop());
    }

    public function testIsEmpty()
    {
        $tower = new Tower();
        $this->assertTrue($tower->isEmpty());

        $disc = new Disc(4);
        $tower->push($disc);
        $this->assertFalse($tower->isEmpty());
    }

    public function testIsFull()
    {
        $tower = new Tower();
        $this->assertFalse($tower->isFull());

        for ($i = 1; $i <= 7; $i++) {
            $tower->push(new Disc($i));
        }

        $this->assertTrue($tower->isFull());
    }

    public function testPeek()
    {
        $tower = new Tower();
        $disc = new Disc(5);
        $tower->push($disc);

        $this->assertEquals($disc, $tower->peek());
    }

    public function testSize()
    {
        $tower = new Tower();
        $this->assertEquals(0, $tower->size());

        $tower->push(new Disc(2));
        $tower->push(new Disc(4));

        $this->assertEquals(2, $tower->size());
    }
}