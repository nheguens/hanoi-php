<?php
// namespace Tests\Integration;

// use PHPUnit\Framework\TestCase;

// require_once 'src/class/disc.php';
// require_once 'src/class/tower.php';
// require_once 'src/class/game_exception.php';
// require_once 'src/class/game.php';

// class GameIntegrationTest extends TestCase
// {
//     public function testGameMove()
//     {
//         $game = new Game();
//         $game->init();

//         // Perform a valid move from tower 1 to tower 2
//         $game->move(1, 2);

//         // Assert that the move was successful
//         $this->assertEquals(1, $game->getTurn());
//         $this->assertFalse($game->isOver());
//         $this->assertInstanceOf(Disc::class, $game->getTower(2)->peek());

//         // Perform an invalid move from tower 1 to tower 3
//         $this->expectException(GameException::class);
//         $game->move(1, 3);
//     }
// }

use PHPUnit\Framework\TestCase;

require_once 'src/class/disc.php';
require_once 'src/class/tower.php';
require_once 'src/class/game_exception.php';
require_once 'src/class/game.php';

class GameIntegrationTest extends TestCase
{
    public function testGameMove()
    {
        $game = new Game();
        $game->init();

        // Perform a valid move from tower 1 to tower 2
        $game->move(1, 2);

        // Assert that the move was successful
        $this->assertEquals(1, $game->getTurn());
        $this->assertFalse($game->isOver());
        $this->assertInstanceOf(Disc::class, $game->getTower(2)->peek());

        // Perform an invalid move from tower 1 to tower 3
        $this->expectException(GameException::class);
        $game->move(1, 3);
    }
}